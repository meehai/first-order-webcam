"""Given two datasets of PNGs in the format
   - datasetPath1
        - 1.png, ..., N.png
        - faceKPs.npy
   - datasetPath2
        - 1.png, ..., M.png
        - faceKPs.npy
    Create a new dataset by combining the two:
    - combinedPath
        - 1.png, ..., N.png (from datasetPath1)
        - N+1.png, ..., N+M.png (from datasetPath2)
        - faceKPs.npy (by combining the arrays)
"""
import numpy as np
from pathlib import Path
from argparse import ArgumentParser
from tqdm import trange
from shutil import copyfile

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("--dataset1Path", required=True)
    parser.add_argument("--dataset2Path", required=True)
    parser.add_argument("--combinedPath", required=True)
    args = parser.parse_args()
    args.dataset1Path = Path(args.dataset1Path).absolute()
    args.dataset2Path = Path(args.dataset2Path).absolute()
    args.combinedPath = Path(args.combinedPath).absolute()
    return args

def sanityChecks(args):
    def sanityCheckDataset(path):
        assert path.exists()
        allFiles = [x for x in path.iterdir()]
        pngs = [x for x in path.glob("*.png")]
        assert len(allFiles) == len(pngs) + 1
        assert (path / "faceKPs.npy").exists()
        kps = np.load(path / "faceKPs.npy")
        assert len(kps) == len(pngs)
        for i in range(len(pngs)):
            assert (path / f"{i}.png").exists()
    sanityCheckDataset(args.dataset1Path)
    sanityCheckDataset(args.dataset2Path)

def main():
    args = getArgs()
    sanityChecks(args)

    kps1 = np.load(args.dataset1Path / "faceKPs.npy")
    kps2 = np.load(args.dataset2Path / "faceKPs.npy")
    nPngs1 = len(kps1)
    nPngs2 = len(kps2)
    print(f"[main] Dataset 1 count: {nPngs1}.")
    print(f"[main] Dataset 2 count: {nPngs2}.")

    args.combinedPath.mkdir(parents=True, exist_ok=False)
    for i in trange(nPngs1):
        inPng1 = args.dataset1Path / ("%d.png" % i)
        outPng1 = args.combinedPath / ("%d.png" % i)
        copyfile(inPng1, outPng1)

    for i in trange(nPngs2):
        inPng2 = args.dataset2Path / f"{i}.png" % i
        outPng2 = args.combinedPath / f"{i + nPngs1}.png"
        copyfile(inPng2, outPng2)
    kps = np.concatenate([kps1, kps2], axis=0)
    np.save(args.combinedPath / "faceKPs.npy", kps)
    print(f"[main] Stored new dataset at {args.combinedPath}. Count: {len(kps)}")

if __name__ == "__main__":
    main()
